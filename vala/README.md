Listen to upower.

https://pythonhosted.org/txdbus/dbus_overview.html

https://chebizarro.gitbooks.io/the-vala-tutorial/content/d-bus-integration.html

https://github.com/freesmartphone/vala-dbus-binding-tool

https://www.piware.de/2009/11/hello-dbus-in-vala/

http://www.dylanmccall.com/blog/2013/07/15/gnome-break-timer-gsoc-project-week-4/

> session_proxy = (FreeDesktopDBus) connection.get_object
> (FreeDesktopDBus.UNIQUE_NAME,
> FreeDesktopDBus.OBJECT_PATH,
> FreeDesktopDBus.INTERFACE_NAME);
>
> session_proxy.name_acquired.connect (this.name_acquired);
What I would have done is:

session_proxy.name_acquired.connect ((name) => this.name_acquired.begin(name));
