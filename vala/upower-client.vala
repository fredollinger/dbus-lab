using DBus;

/* Interface name */
[DBus (name = "org.freedesktop.UPower")]
interface UPower_if : GLib.Object {
    public abstract GLib.ObjectPath[] EnumerateDevices () throws IOError;
    public signal void DataChanged (); // org/freedesktop/UPower/Wakeups
    public abstract double Percentage() throws IOError;
    public abstract bool OnBattery() throws IOError;
    //public signal void DeviceAdded (DBus.Object Path); // org/freedesktop/UPower
}

/*
void EnumerateDevices() {
    // Important: keep demo variable out of try/catch scope not lose signals!
    UPower_if upower = null;

    try {
        upower = GLib.Bus.get_proxy_sync<UPower_if> (GLib.BusType.SYSTEM, "org.freedesktop.UPower",
                                                    "/org/freedesktop/UPower");
        var reply = upower.EnumerateDevices ();

        foreach (string item in reply) {
            stdout.printf("%s, \n", item);
        }
    } catch (IOError e) {
        stderr.printf ("%s\n", e.message);
    }
}
*/

/*
void GetProperty() {
    UPower_if upower = null;
    upower = GLib.Bus.get_proxy_sync<UPower_if> (GLib.BusType.SYSTEM, "org.freedesktop.UPower",
                                                    "/org/freedesktop/UPower");
    //var con = DBus.Bus.get(DBus.BusType.SYSTEM);
    //dynamic DBus.Object obj = con.get_object("org.freedesktop.UPower", "/org/freedesktop/UPower/devices/battery_BAT0", "org.freedesktop.DBus.Properties");
    //double pct = upower.Percentage();
    bool onbat = upower.OnBattery();
    stdout.printf("%i, \n", (int)onbat);
}
*/

void SignalConnect() {
    UPower_if upower = null;
    upower = GLib.Bus.get_proxy_sync<UPower_if> (GLib.BusType.SYSTEM, "org.freedesktop.UPower",
                                                    "/org/freedesktop/UPower");

    // http://osdir.com/ml/vala-list/2010-12/msg00012.html
    upower.DataChanged.connect(() => {
    });

}

void main () {
    // var loop = new MainLoop();
    SignalConnect();
    //EnumerateDevices();
    //GetProperty();
    //loop.run();
}
